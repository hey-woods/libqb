Name:                libqb
Version:             1.0.5
Release:             2
Summary:             Library providing high performance logging, tracing, ipc, and poll
License:             LGPLv2+
URL:                 https://github.com/ClusterLabs/libqb
Source0:             https://github.com/ClusterLabs/libqb/releases/download/v%{version}/%{name}-%{version}.tar.xz
Patch0:              IPC-avoid-temporary-channel-priority-loss.patch
Patch1:              libqb-fix-list-handling-gcc10.patch
Patch2:		     fix-corosync-compile-fail.patch
BuildRequires:       autoconf automake libtool check-devel doxygen gcc procps pkgconfig(glib-2.0)
BuildRequires:       git-core
%description
libqb provides high-performance, reusable features for client-server
architecture, such as logging, tracing, inter-process communication (IPC),
and polling.

%prep
%autosetup -p1 -S git_am

%build
./autogen.sh
%configure --disable-static
%{make_build}

%if 0%{?with_check}
%check
make check V=1 \
  && make -C tests/functional/log_internal check V=1
%endif

%install
%{make_install}
find $RPM_BUILD_ROOT -name '*.la' -delete
rm -rf $RPM_BUILD_ROOT/%{_docdir}/*
%ldconfig_scriptlets

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release} pkgconfig
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package        help
Summary:        help documents for libqb package
Buildarch:      noarch
%description    help
help documents for libqb package

%files
%license COPYING
%{_sbindir}/qb-blackbox
%{_libdir}/libqb.so.*

%files          devel
%doc README.markdown
%{_includedir}/qb/
%{_libdir}/libqb.so
%{_libdir}/pkgconfig/libqb.pc

%files          help
%{_mandir}/man8/qb-blackbox.8*
%{_mandir}/man3/qb*3*

%changelog
* Thu Mar 4 2021 baizhonggui <baizhonggui@huawei.com> - 1.0.5-2
- new add #include <stddef.h> in qblist.h to fix:
  issue# https://gitee.com/src-openeuler/corosync/issues/I39X3F?from=project-issue
  upstream link:https://github.com/ClusterLabs/libqb/pull/384

* Thu Feb 25 2021 sunguoshuai <sunguoshuai@huawei.com> - 1.0.5-1
- upgrade to 1.0.5

* Tue Feb 8 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 1.0.3-7
- Fix CVE-2019-12779 libqb before 1.0.5 allows local users to overwrite arbitrary files via a symlink attack.
  CVE Link: https://nvd.nist.gov/vuln/detail/CVE-2019-12779
  Community Patch Link:
  https://github.com/ClusterLabs/libqb/commit/e322e98dc264bc5911d6fe1d371e55ac9f95a71e
  https://github.com/ClusterLabs/libqb/commit/7cd7b06d52ac80c343f362c7e39ef75495439dfc
  https://github.com/ClusterLabs/libqb/commit/6a4067c1d1764d93d255eccecfd8bf9f43cb0b4d

* Tue Apr 27 2020 wangerfeng <wangerfeng5@huawei.com> - 1.0.3-6
- Package init
